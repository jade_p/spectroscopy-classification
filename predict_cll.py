import os
import pandas as pd
import matplotlib.pyplot as plt
import random
import numpy as np
import logging
from tqdm import tqdm
from sklearn.utils.class_weight import compute_class_weight
from sklearn.preprocessing import StandardScaler
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.layers import LeakyReLU
from keras.metrics import Recall

df = pd.read_csv('raw_data.csv', delimiter=';')
mu, sigma = 0, 40 / 2.35
np.random.normal(mu, sigma, 999)


def make_dirs():
    """
    Creates local directories for results
    """
    if not os.path.isdir('models'):
        os.mkdir('models')
    if not os.path.isdir('results'):
        os.mkdir('results')


def remove_outliers(df):
    """
    Removes outliers explained in the presentation, from data exploration phase
    """
    indexes_to_remove = []
    conds = [('lambda_6', 500), ('lambda_97', 1000), ('lambda_10', 500)]

    for cond in conds:
        lambda_cols_rm = [col for col in df.columns if cond[0] in col and len(col) == 10]
        mask = df[lambda_cols_rm].gt(cond[1]).any(axis=1)
        indexes = np.where(mask)[0]
        for index in indexes:
            indexes_to_remove.append(index)
    df = df.drop(indexes_to_remove)

    return df


def select_train_patient_ids(patient_state, df, train_patient_ids):
    """
    Select train and test patients for
    """
    state_patients = df.loc[df['patient_state'] == patient_state]
    unique_cells = state_patients.drop_duplicates('cell_name')

    cells_per_patient = unique_cells.groupby('patient_name').count()['cell_name']
    total_cells = cells_per_patient.sum()
    train_size_limit = int(0.8 * total_cells)
    filled_train_size = 0
    patients_list = cells_per_patient.index.values
    random.shuffle(patients_list)
    for patient_id in patients_list:
        patient_cells_number = cells_per_patient[patient_id]
        if filled_train_size < train_size_limit:
            train_patient_ids.append(patient_id)
            filled_train_size += patient_cells_number
    return train_patient_ids


def split_patients(df, patient_states):
    """
    split patients into train and test sets with stratification on patient state
    """
    train_patient_ids = []

    for patient_state in patient_states:
        train_patient_ids = select_train_patient_ids(patient_state, df, train_patient_ids)

    full_patients_list = df.patient_name.unique()

    test_patient_ids = [patient_id for patient_id in full_patients_list if not patient_id in train_patient_ids]
    return train_patient_ids, test_patient_ids


def generate_cells_mean_dict(df):
    """
    create cells dict with info for each cell and compute mean signal for each cell over the existing spectra
    """
    cells_list = df.cell_name.unique()
    cells_dict_to_df = {}
    lambda_cols = [col for col in df.columns if 'lambda_' in col]

    for cell_name in tqdm(cells_list):
        cell_subdf = df.loc[df['cell_name'] == cell_name]
        cell_row = []
        full_row = []
        for _, row in cell_subdf.iterrows():
            cell_row.append(row[lambda_cols].values)
        mean_row = np.mean(cell_row, axis=0)
        full_row.append(mean_row)
        full_row = [item for sublist in full_row for item in sublist]
        cells_dict_to_df[cell_name] = full_row
    return cells_dict_to_df


def generate_cells_df(cells_dict):
    """
    generate cells df from dictionary
    """
    first_key = list(cells_dict.keys())[0]
    length = len(cells_dict[first_key])
    cells_df_columns = ['lambda_{}'.format(i + 1) for i in range(length)]
    cells_df = pd.DataFrame.from_dict(cells_dict, orient='index', columns=cells_df_columns)
    return cells_df


def get_df_ids(cells_df):
    """
    recompute the patient id to predict on patient for test

    """
    cells_ids = cells_df.index
    patient_ids = [cell_id.split('_')[0] + '_' + cell_id.split('_')[1] for cell_id in cells_ids]
    return patient_ids


def generate_output_column(df):
    """
    process output column
    """
    cell_first_df = df.groupby('cell_name').first().reset_index()
    cell_first_df = cell_first_df[['cell_name', 'cell_type']]
    cell_first_df.cell_type[cell_first_df.cell_type == 'B'] = 1
    cell_first_df.cell_type[cell_first_df.cell_type == 'TNK'] = 0

    cells_Y = cell_first_df.set_index('cell_name')
    return cells_Y


def merge_dfs(df_X, df_Y):
    """
    merge dweight_dictataframes via index
    """
    return pd.merge(df_X, df_Y, left_index=True, right_index=True)



def augment_data(full_df, train_patient_ids, augment_proportion=10):
    """
    augment data with random noise as defined in the protocol
    """
    lambda_cols = [col for col in full_df.columns if 'lambda_' in col]
    augmented_df = full_df.copy()
    all_indexes = full_df.index
    train_indexes = [index for index in all_indexes if any(patient in index for patient in train_patient_ids)]
    for index, row in tqdm(full_df.loc[train_indexes].iterrows()):
        this_lambdas = row[lambda_cols].values
        for i in range(augment_proportion):
            new_lambdas = this_lambdas + np.random.normal(mu, sigma, 999)
            new_row = np.append(new_lambdas, row['cell_type'])
            augmented_df = augmented_df.append(dict(zip(augmented_df.columns, new_row)), ignore_index=True)
    return augmented_df


def add_indexes(augmented_df, full_df):
    """
    add cells indexes to the augmented df to identify test cells a posteriori
    """
    cells_index = full_df.index.values
    new_index = np.append(cells_index, augmented_df.index.values[len(full_df):])
    augmented_df.index = new_index
    return augmented_df


def standardize_input(augmented_df):
    """
    standardize the input columns with the spectrum of the dataset
    """
    lambda_cols = [col for col in augmented_df.columns if 'lambda_' in col]
    augmented_df[lambda_cols] = StandardScaler().fit_transform(augmented_df[lambda_cols])
    return augmented_df


def split_df(augmented_df, test_patient_ids):
    """
    Split df into train and test sets for training
    """
    lambda_cols = [col for col in augmented_df.columns if 'lambda_' in col]

    indices = [s for i, s in enumerate(augmented_df.index) if
               type(s) == str and any(patient in s for patient in test_patient_ids)]

    df_test = augmented_df.loc[indices]

    bad_cond = augmented_df.index.isin(indices)
    df_train = augmented_df.iloc[~bad_cond]

    X_train = df_train[lambda_cols]
    X_test = df_test[lambda_cols]
    y_train = df_train['cell_type']
    y_test = df_test['cell_type']
    return X_train, X_test, y_train, y_test


def generate_dataset_balance_dict(full_df):
    """
    Generate dictionary with balance weights snce the dataset is imbalanced
    """
    weight_array = compute_class_weight('balanced', np.unique(full_df.cell_type),
                                        full_df.cell_type)  # y is the array containing your labels

    weight_dict = dict(zip(np.unique(full_df.cell_type), weight_array))
    return weight_dict


def reshape_features(X_train, X_test, y_train, y_test):
    """
    Reshape features for neural net input
    """
    X_train = X_train.values.reshape(X_train.shape[0], X_train.shape[1], 1)
    X_test = X_test.values.reshape(X_test.shape[0], X_test.shape[1], 1)
    y_train = np.asarray(y_train).astype('float32')
    y_test = np.asarray(y_test).astype('float32')
    return X_train, X_test, y_train, y_test


def create_model(input_shape):
    """
    Create ConvNet model
    """
    model = Sequential()
    model.add(Conv1D(32, kernel_size=3, activation='relu', input_shape=input_shape))
    model.add(LeakyReLU(alpha=0.1))
    model.add(Conv1D(64, kernel_size=3, activation='relu'))
    model.add(LeakyReLU(alpha=0.1))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy', Recall()])
    return model


def train_model(model, X_train, X_test, y_train, y_test, weight_dict, epochs=2):
    """
    Train model
    """
    history = model.fit(X_train, y_train, batch_size=128, epochs=epochs, verbose=1, validation_data=(X_test, y_test),
                        class_weight=weight_dict)
    model.save('models')
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig('results/model_accuracy.png')
    plt.clf()

    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig('results/model_loss.png')

    return model


def predict_test_patients(model, X_test):
    """
    Predict cell category for inference
    """
    preds = model.predict(X_test)
    return preds


def classify(predicted_state, ground_truth, tp, tn, fp, fn):
    """
    classify the case as tp, tn, fp or fn
    """
    if predicted_state == "malade" and ground_truth == "malade":
        tp += 1
    elif predicted_state == "malade" and ground_truth == "sain":
        fp += 1
    elif predicted_state == "sain" and ground_truth == 'sain':
        tn += 1
    elif predicted_state == 'sain' and ground_truth == 'malade':
        fn += 1
    return tp, tn, fp, fn


def precision_recall(tp, fp, fn):
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    return precision, recall


def process_predictions(predictions, df, X_test, test_patient_ids, prediction_results, prediction_performance, i):
    test_indices = X_test.index.values
    tp = 0
    tn = 0
    fn = 0
    fp = 0
    S1 = 0.45
    S2 = 0.48

    for test_patient_id in test_patient_ids:
        test_patient_cells_indices = [i for i, s in enumerate(test_indices) if test_patient_id in s]
        patient_predictions = [predictions[i] for i in test_patient_cells_indices]
        predicted_b = [pred for pred in patient_predictions if pred > S1]
        total_cells = len(patient_predictions)
        proportion_b = len(predicted_b) / total_cells
        if proportion_b > S2:
            predicted_state = "malade"
        else:
            predicted_state = 'sain'
        ground_truth = df.loc[df['patient_name'] == test_patient_id, 'patient_state'].values[0]
        tp, tn, fp, fn = classify(predicted_state, ground_truth, tp, tn, fp, fn)
        logging.info("The patient was " + str(ground_truth) + "and he was predicted " + str(predicted_state))
        prediction_results = prediction_results.append({'test_patient_id': test_patient_id,
                                                        'ground_truth': ground_truth,
                                                        'prediction': predicted_state},
                                                       ignore_index=True)
    precision, recall = precision_recall(tp, fp, fn)
    prediction_performance = prediction_performance.append({'test_index': i, 'precision': precision, 'recall': recall},
                                                           ignore_index=True)
    prediction_performance.to_csv('results/model_performance.csv')
    prediction_results.to_csv('results/model_predictions.csv')
    return prediction_results, prediction_performance


def run_experiment(df, iterations=10):
    make_dirs()
    patient_states = df['patient_state'].unique()

    df = remove_outliers(df)
    cells_dict = generate_cells_mean_dict(df)
    cells_df = generate_cells_df(cells_dict)
    cells_Y = generate_output_column(df)
    full_df = merge_dfs(cells_df, cells_Y)
    weight_dict = generate_dataset_balance_dict(full_df)

    prediction_results = pd.DataFrame(columns=['test_patient_id', 'ground_truth', 'prediction'])
    prediction_performance = pd.DataFrame(columns=['test_index', 'precision', 'recall'])
    for i in range(iterations):
        train_patient_ids, test_patient_ids = split_patients(df, patient_states)

        augmented_df = augment_data(full_df, train_patient_ids, augment_proportion=2)
        augmented_df = add_indexes(augmented_df, full_df)
        augmented_df = standardize_input(augmented_df)
        X_train, X_test, y_train, y_test = split_df(augmented_df, test_patient_ids)
        x_train, x_test, y_train, y_test = reshape_features(X_train, X_test, y_train, y_test)

        CNN = create_model(input_shape=(x_train.shape[1], x_train.shape[2]))
        CNN = train_model(CNN, x_train, x_test, y_train, y_test, weight_dict, epochs=10)

        test_predictions = predict_test_patients(CNN, x_test)
        prediction_results, prediction_performance = process_predictions(test_predictions,
                                                                         df,
                                                                         X_test,
                                                                         test_patient_ids,
                                                                         prediction_results,
                                                                         prediction_performance,
                                                                         i)


run_experiment(df)
