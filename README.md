# CLL Diagnosis
This project is composed of an analysis of the data which can be visualized through a jupyter notebook and the presentation, a code for comparing different models and the code for the chosen model and results.
To compute the results, please follow the following steps :
- open the project directory and install required librairies

pip3 install requirements.txt
   
- open the analysis

jupyter notebook

- run the models comparison
python3 compare_models.py
   
- run the predictions

python3 predict_cll.py