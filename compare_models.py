import os
import pandas as pd
import random
import numpy as np
import logging
from tqdm import tqdm
from sklearn.utils.class_weight import compute_class_weight
from sklearn.preprocessing import StandardScaler
from sklearn import model_selection
from sklearn.model_selection import GridSearchCV
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import GradientBoostingClassifier

df = pd.read_csv('raw_data.csv', delimiter=';')
mu, sigma = 0, 40 / 2.35
np.random.normal(mu, sigma, 999)
model_names = ["Nearest Neighbors", "Linear SVM",
               "Decision Tree", "Random Forest", "Neural Net", "AdaBoost",
               "Naive Bayes", "GradientBoosting"]

classifiers = [
    KNeighborsClassifier(3),
    SVC(kernel="linear", C=0.025),
    DecisionTreeClassifier(max_depth=5),
    RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1),
    MLPClassifier(alpha=1, max_iter=1000),
    AdaBoostClassifier(),
    GaussianNB(),
    GradientBoostingClassifier(n_estimators=100, learning_rate=0.01,
                               max_depth=7, random_state=0)]
param_grid = {'C': [0.1, 1, 10, 100],
                      'gamma': [1, 0.1, 0.01, 0.001, 0.0001]}


def make_dirs():
    """
    Creates local directories for results
    """
    if not os.path.isdir('models'):
        os.mkdir('models')
    if not os.path.isdir('results'):
        os.mkdir('results')


def remove_outliers(df):
    """
    Removes outliers explained in the presentation, from data exploration phase
    """
    indexes_to_remove = []
    conds = [('lambda_6', 500), ('lambda_97', 1000), ('lambda_10', 500)]

    for cond in conds:
        lambda_cols_rm = [col for col in df.columns if cond[0] in col and len(col) == 10]
        mask = df[lambda_cols_rm].gt(cond[1]).any(axis=1)
        indexes = np.where(mask)[0]
        for index in indexes:
            indexes_to_remove.append(index)
    df = df.drop(indexes_to_remove)

    return df


def select_train_patient_ids(patient_state, df, train_patient_ids):
    """
    Select train and test patients for
    """
    state_patients = df.loc[df['patient_state'] == patient_state]
    unique_cells = state_patients.drop_duplicates('cell_name')

    cells_per_patient = unique_cells.groupby('patient_name').count()['cell_name']
    total_cells = cells_per_patient.sum()
    train_size_limit = int(0.8 * total_cells)
    filled_train_size = 0
    patients_list = cells_per_patient.index.values
    random.shuffle(patients_list)
    for patient_id in patients_list:
        patient_cells_number = cells_per_patient[patient_id]
        if filled_train_size < train_size_limit:
            train_patient_ids.append(patient_id)
            filled_train_size += patient_cells_number
    return train_patient_ids


def split_patients(df, patient_states):
    """
    split patients into train and test sets with stratification on patient state
    """
    train_patient_ids = []

    for patient_state in patient_states:
        train_patient_ids = select_train_patient_ids(patient_state, df, train_patient_ids)

    full_patients_list = df.patient_name.unique()

    test_patient_ids = [patient_id for patient_id in full_patients_list if not patient_id in train_patient_ids]
    return train_patient_ids, test_patient_ids


def generate_cells_mean_dict(df):
    """
    create cells dict with info for each cell and compute mean signal for each cell over the existing spectra
    """
    cells_list = df.cell_name.unique()
    cells_dict_to_df = {}
    lambda_cols = [col for col in df.columns if 'lambda_' in col]

    for cell_name in tqdm(cells_list):
        cell_subdf = df.loc[df['cell_name'] == cell_name]
        cell_row = []
        full_row = []
        for _, row in cell_subdf.iterrows():
            cell_row.append(row[lambda_cols].values)
        mean_row = np.mean(cell_row, axis=0)
        full_row.append(mean_row)
        full_row = [item for sublist in full_row for item in sublist]
        cells_dict_to_df[cell_name] = full_row
    return cells_dict_to_df


def generate_cells_df(cells_dict):
    """
    generate cells df from dictionary
    """
    first_key = list(cells_dict.keys())[0]
    length = len(cells_dict[first_key])
    cells_df_columns = ['lambda_{}'.format(i + 1) for i in range(length)]
    cells_df = pd.DataFrame.from_dict(cells_dict, orient='index', columns=cells_df_columns)
    return cells_df


def get_df_ids(cells_df):
    """
    recompute the patient id to predict on patient for test

    """
    cells_ids = cells_df.index
    patient_ids = [cell_id.split('_')[0] + '_' + cell_id.split('_')[1] for cell_id in cells_ids]
    return patient_ids


def generate_output_column(df):
    """
    process output column
    """
    cell_first_df = df.groupby('cell_name').first().reset_index()
    cell_first_df = cell_first_df[['cell_name', 'cell_type']]
    cell_first_df.cell_type[cell_first_df.cell_type == 'B'] = 1
    cell_first_df.cell_type[cell_first_df.cell_type == 'TNK'] = 0

    cells_Y = cell_first_df.set_index('cell_name')
    return cells_Y


def merge_dfs(df_X, df_Y):
    """
    merge dataframes via index
    """
    return pd.merge(df_X, df_Y, left_index=True, right_index=True)


def augment_data(full_df, train_patient_ids, augment_proportion=10):
    """
    augment data with random noise as defined in the protocol
    """
    lambda_cols = [col for col in full_df.columns if 'lambda_' in col]
    augmented_df = full_df.copy()
    all_indexes = full_df.index
    train_indexes = [index for index in all_indexes if any(patient in index for patient in train_patient_ids)]
    for index, row in tqdm(full_df.loc[train_indexes].iterrows()):
        this_lambdas = row[lambda_cols].values
        for i in range(augment_proportion):
            new_lambdas = this_lambdas + np.random.normal(mu, sigma, 999)
            new_row = np.append(new_lambdas, row['cell_type'])
            augmented_df = augmented_df.append(dict(zip(augmented_df.columns, new_row)), ignore_index=True)
    return augmented_df


def add_indexes(augmented_df, full_df):
    """
    add cells indexes to the augmented df to identify test cells a posteriori
    """
    cells_index = full_df.index.values
    new_index = np.append(cells_index, augmented_df.index.values[len(full_df):])
    augmented_df.index = new_index
    return augmented_df


def standardize_input(augmented_df):
    """
    standardize the input columns with the spectrum of the dataset
    """
    lambda_cols = [col for col in augmented_df.columns if 'lambda_' in col]
    augmented_df[lambda_cols] = StandardScaler().fit_transform(augmented_df[lambda_cols])
    return augmented_df


def split_df(augmented_df, test_patient_ids):
    """
    Split df into train and test sets for training
    """
    lambda_cols = [col for col in augmented_df.columns if 'lambda_' in col]

    indices = [s for i, s in enumerate(augmented_df.index) if
               type(s) == str and any(patient in s for patient in test_patient_ids)]

    df_test = augmented_df.loc[indices]

    bad_cond = augmented_df.index.isin(indices)
    df_train = augmented_df.iloc[~bad_cond]

    X_train = df_train[lambda_cols]
    X_test = df_test[lambda_cols]
    y_train = df_train['cell_type']
    y_test = df_test['cell_type']
    return X_train, X_test, y_train, y_test


def generate_dataset_balance_dict(full_df):
    """
    Generate dictionary with balance weights snce the dataset is imbalanced
    """
    weight_array = compute_class_weight('balanced', np.unique(full_df.cell_type),
                                        full_df.cell_type)  # y is the array containing your labels

    weight_dict = dict(zip(np.unique(full_df.cell_type), weight_array))
    return weight_dict


def reshape_features(X_train, X_test, y_train, y_test):
    """
    Reshape features for neural net input
    """
    X_train = X_train.values.reshape(X_train.shape[0], X_train.shape[1], 1)
    X_test = X_test.values.reshape(X_test.shape[0], X_test.shape[1], 1)
    y_train = np.asarray(y_train).astype('float32')
    y_test = np.asarray(y_test).astype('float32')
    return X_train, X_test, y_train, y_test


def compare_models(model_names, classifiers, X_train, X_test, y_train, y_test):

    scoring = 'accuracy'
    results = []
    names = []
    scores = []
    seed = 7
    for name, clf in zip(model_names, classifiers):
        clf.fit(X_train, y_train)
        score = clf.score(X_test, y_test)
        kfold = model_selection.KFold(n_splits=10, random_state=seed)
        cv_results = model_selection.cross_val_score(clf, X_train, y_train, cv=kfold, scoring=scoring)
        results.append(cv_results)
        names.append(name)
        scores.append(score)
        msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
        logging.warning(msg)


def grid_search_svc(param_grid, X_train, y_train):
    grid = GridSearchCV(SVC(kernel='linear'), param_grid, refit=True, verbose=3)

    grid.fit(X_train, y_train)
    logging.warning(grid.best_score_)
    logging.warning(grid.best_params_)


def run_comparison(df, iterations=2):
    make_dirs()
    patient_states = df['patient_state'].unique()

    df = remove_outliers(df)
    cells_dict = generate_cells_mean_dict(df)
    cells_df = generate_cells_df(cells_dict)
    cells_Y = generate_output_column(df)
    full_df = merge_dfs(cells_df, cells_Y)

    for i in range(iterations):
        train_patient_ids, test_patient_ids = split_patients(df, patient_states)

        augmented_df = augment_data(full_df, train_patient_ids, augment_proportion=0)
        augmented_df = add_indexes(augmented_df, full_df)
        augmented_df = standardize_input(augmented_df)
        X_train, X_test, y_train, y_test = split_df(augmented_df, test_patient_ids)
        x_train, x_test, y_train, y_test = reshape_features(X_train, X_test, y_train, y_test)
        #compare_models(model_names, classifiers, X_train, X_test, y_train, y_test)
        grid_search_svc(param_grid, X_train, y_train)


run_comparison(df)
